from debconf.common_settings import *

INSTALLED_APPS = (
    'badges',
    'debconf',
    'exports',
    'bursary',
    'front_desk',
    'invoices',
    'register',
) + INSTALLED_APPS
