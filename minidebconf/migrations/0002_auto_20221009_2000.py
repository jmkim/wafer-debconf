# Generated by Django 2.2.28 on 2022-10-09 20:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('minidebconf', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Diet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('option', models.CharField(max_length=32, verbose_name='Diet')),
            ],
        ),
        migrations.CreateModel(
            name='ShirtSize',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=32, verbose_name='Description')),
            ],
        ),
        migrations.AlterModelOptions(
            name='registration',
            options={'verbose_name': 'registration', 'verbose_name_plural': 'registrations'},
        ),
        migrations.AlterField(
            model_name='registration',
            name='gender',
            field=models.CharField(blank=True, choices=[('', 'Decline to state'), ('m', 'Male'), ('f', 'Female'), ('n', 'Non-Binary')], max_length=1, null=True, verbose_name='Gender'),
        ),
        migrations.AddField(
            model_name='registration',
            name='diet',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='minidebconf.Diet'),
        ),
        migrations.AddField(
            model_name='registration',
            name='shirt_size',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='minidebconf.ShirtSize'),
        ),
    ]
